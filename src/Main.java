import java.awt.Dimension;

import javax.swing.JFrame;

public class Main {
	
	public static void main(String [] args) {
		
		JFrame frame = new JFrame("PROJECT-SCH");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setPreferredSize(new Dimension(800, 800));
		
		//Voronoi v = new Voronoi(Voronoi.loadKovetelmenyMap());
		
		Voronoi v = new Voronoi();
		v.generate(64, 80);
		
		frame.add(v);
		frame.pack();
		
		//centers the frame on screen
		frame.setLocationRelativeTo(null);
		
		frame.setResizable(false);
		frame.setVisible(true);
		
	}	

}
