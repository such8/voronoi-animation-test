import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.HashSet;

/**
 * Brute force
 * A pályagerenálás ideje a pálya (pixelben értelmezett) felbontásától függ. 
 * @author Mate
 */
public class VoronoiCell {
	private int xStarting, yStarting; //Cell origin point (!= center point)
	private BufferedImage canvas;
	private Color cellColor;
	private Rectangle bounds;
	private int delay;
	private boolean finished = false;
	private HashSet<Point> pixels = new HashSet<Point>();
	private HashSet<VoronoiCell> neighbours = new HashSet<VoronoiCell>();
	private boolean disabled = false;
	
	
	public VoronoiCell(BufferedImage _canvas, Point _startingPoint, Color _cellColor, int _age) {
		canvas = _canvas;
		xStarting = _startingPoint.x;
		yStarting = _startingPoint.y;
		cellColor = _cellColor;
		bounds = canvas.getData().getBounds();
		delay = _age;
	}
	
	public Point getStartingPoint() {
		return new Point(xStarting, yStarting);
	}
	
	public void addNeighbour(VoronoiCell c) {
		neighbours.add(c);
	}
	
	public HashSet<VoronoiCell> getNeighbours() {
		return neighbours;
	}
	
	public HashSet<Point> getPixels() {
		return pixels;
	}
	
	
	//TODO ez nem tudom mennyire OO lehet át kéne irni
	/**
	 * Inaktívvá teszi a cellát, hogy ne lehessen rálépni.
	 * (A fekete színű "blocker" cellálhoz)
	 */
	public void disable() {
		disabled = true;
	}
	
	/**
	 * 
	 * @return Igazzal tér vissza, ha a cella inaktív állapotban van.
	 */
	public boolean isDisabled() {
		return disabled;
	}
	
	
	/**
	 * Voronoi cella terjeszkedése.
	 * Mielött rajzolna egy kört, megvizsgálja, hogy terjeszkedhet-e arra.
	 * @param distance A cella distance távolságnyi pixelre terjeszkedett a kezdőpontjától.
	 * Folyamatosan növekcő paraméterel kell hívni.
	 * @return Igazzal tér vissza, ha már elkészült, azaz semmilyen irányban sem tud tovább terjeszkedni.
	 */
	public boolean drawCell(int distance)  {
		if (finished) {
			return true;
		}
		if (distance < delay) {
			return false;
		}
		int radius = distance - delay;
		boolean hasDrawn = false;
		Point drawnPixel = new Point();
		System.out.println("drawing");
		
		Graphics2D g = canvas.createGraphics();
		g.setColor(Color.CYAN);
		g.drawOval(xStarting, yStarting, 3, 3);
			
			//bejárja a 2*radius oldalú négyzetet
			for (int y = -radius; y <= radius; y++) {
				for (int x = -radius; x <= radius; x++) {
					//Ha a körön belül van
					if (x*x+y*y <= radius * radius) {
						//Ha a pályán belül van és még nincs kiszinezve
						if (bounds.contains(xStarting + x, yStarting + y) && canvas.getRGB(xStarting + x, yStarting + y) == Color.OPAQUE) {
							canvas.setRGB(xStarting + x, yStarting + y, cellColor.getRGB());
							drawnPixel = new Point(xStarting + x, yStarting + y);
							pixels.add(drawnPixel);
							hasDrawn = true;
						}
					}
				}
			}
			if(!hasDrawn) {
				finished = true;
				return true;
			}
			return false;
		}
	
	/**
	 * Kirajzolja a cellát egy világosabb színnel (amennyiben ilyen létezik)
	 */
	public void highlight() {
		if(disabled) {
			return;
		}
		for (Point p : pixels) {
			canvas.setRGB(p.x, p.y, cellColor.brighter().getRGB());
		}
	}
	
	/**
	 * Kirajzolja a cellát a megadott színnel.
	 * Egy paraméter nélküli redraw() hívással visszaáll az eredeti színre.
	 * @param c A szín amivel a cella ki lesz rajzolva.
	 */
	public void redraw(Color c) {
		if (disabled) {
			return;
		}
		for (Point p : pixels) {
			canvas.setRGB(p.x, p.y, c.getRGB());
		}
	}
	
	/**
	 * Újrarajzolja a cellát az konstruktorban megadott színnel
	 */
	public void redraw() {
		for (Point p : pixels) {
			canvas.setRGB(p.x, p.y, cellColor.getRGB());
		}
	}

}
