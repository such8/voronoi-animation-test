import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Voronoi extends JPanel implements ActionListener, MouseListener,  MouseMotionListener{
	private static final int SIZE = 800;
	private static final int MIN_DISTANCE = SIZE / 10;
	private static final int TIMER_INTERVAL = 16; //TODO jobb lenne delta időt számolni és az alapján animálni
	
	private static BufferedImage canvas = new BufferedImage(SIZE, SIZE, BufferedImage.TYPE_INT_ARGB);
	private ArrayList<VoronoiCell> cells = new ArrayList<VoronoiCell>();
	private Timer timer;
	private int distance = 1;
	
	public Voronoi() {
		//Az egész pályát átlátszó pixelekkel tölti fel
		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < SIZE; j++) {
				canvas.setRGB(i, j, Color.OPAQUE);
			}
		}
		
		addMouseListener(this);
		addMouseMotionListener(this);
		timer = new Timer(TIMER_INTERVAL, this);
		timer.start();
	}
	
	public Voronoi(ArrayList<VoronoiCell> cellList) {
		this();
		cells = cellList;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(canvas, 0, 0, SIZE, SIZE, null);
		g.setColor(Color.WHITE);
		for (int i = 0; i < cells.size(); i++) {
			g.drawString(String.format("%d", i+1), cells.get(i).getStartingPoint().x, cells.get(i).getStartingPoint().y);
		}
		
	}
	
	//Egy véletlengenerált színt ad vissza.
	private Color randomColor(Random rand) {
		int r = rand.nextInt(256);
		int g = rand.nextInt(256);
		int b = rand.nextInt(256);
		
		return new Color(r, g, b);
	}
	
	/**
	 * Megadott feltételek mellett random generál kezdőpontokat egy pályához
	 * @param maxFails Maximum ennyi sikertelen próbálkozás után áll le. Minél nagyobb az érték, annál konzisztensebbek a mezőméretek
	 * @return A generált pontok listája.
	 */
	private List<Point> generateStartingPoints(int maxFails) {
		ArrayList<Point> startingPoints = new ArrayList<Point>();
		Random rand = new Random();
		
		startingPoints.add(new Point(rand.nextInt(SIZE), rand.nextInt(SIZE)));
		boolean tooClose;
		int fails = 0;
		
		while(fails < maxFails) {
			Point randPoint = new Point(rand.nextInt(SIZE), rand.nextInt(SIZE));
			tooClose = false;
			
			for (Point p : startingPoints) {
				if (randPoint.distance(p) < MIN_DISTANCE) {
					tooClose = true;
					fails++;
					break;
				}	
			}
			if (!tooClose) {
				startingPoints.add(randPoint);
				fails = 0;
			}
		}
		
		return startingPoints;
	}
	
	/**
	 * A végigmegy a cellákon, és mindegyiknek noveli a méretét, amennyiben tudja.
	 * @return Igazzal tér vissza, ha a pályagenerálás befejeződött, azaz ha már egyik cella méretét sem tudja növelni.
	 */
	public boolean expandCells()  {
		int cellsDone = 0;
			for (int i = 0; i < cells.size(); i++) {
				VoronoiCell c = cells.get(i);
				if(c.drawCell(distance)) {
					cellsDone++;
				}
			}
			if (cellsDone >= cells.size()) {
				return true;
			}
			return false;
		}
	
	/**
	 * Létrehozza a Voronoi cellákat. amikből a pálya felépül.
	 * @param maxFails A cellák kezdőpontját véletlen generálja, minél nagyobb az érték, annál konzisztensebb a cellaméret.
	 * @param delayRange A cellák indulási időpontját véletlen választja 0 és delayRange között. Minél kisebb az érték, annál szabályosabbak a cellák.
	 */
	public void generate(int maxFails, int delayRange) {
		ArrayList<Point> inputPoints = new ArrayList<Point>(generateStartingPoints(maxFails));
		generate(inputPoints, delayRange);
		
	}
	
	/**
	 * Létrehozza a Voronoi cellákat amikből a pálya felépül
	 * @param inputPoints A cellák kezdőpontjait tartalmazó lista.
	 * @param delayRange A cellák indulási időpontját véletlen választja 0 és delayRange között. Minél kisebb az érték, annál szabályosabbak a cellák.
	 */
	public void generate(List<Point> inputPoints, int delayRange) {
		Random rand = new Random();
		
		for (Point p : inputPoints) {
			cells.add(new VoronoiCell(canvas, p, randomColor(rand), rand.nextInt(delayRange)));
		}
	}
	
	
	/** 
	 * TODO Ezt faljbol kellene beolvasni
	 * Kötelezo pálya kézzel megírva :((
	 * (kurva nehez volt ezeket a koordinatakt kiszamolni)
	 * @return
	 */
	public static ArrayList<VoronoiCell> loadKovetelmenyMap() {
		ArrayList<VoronoiCell> vcells = new ArrayList<VoronoiCell>();
		int small = 25;
		int medium = 15;
		int large = 0;
		
		//tiles -- sorrendben van!!
		vcells.add(new VoronoiCell(canvas, new Point(40,40), Color.BLUE, small));
		vcells.add(new VoronoiCell(canvas, new Point(90,140), Color.RED, large));
		vcells.add(new VoronoiCell(canvas, new Point(20,220), Color.GREEN, small));
		vcells.add(new VoronoiCell(canvas, new Point(120,40), Color.GREEN, small));
		vcells.add(new VoronoiCell(canvas, new Point(180,90), Color.BLUE, small));
		vcells.add(new VoronoiCell(canvas, new Point(190,170), Color.GREEN, small));
		vcells.add(new VoronoiCell(canvas, new Point(150,230), Color.YELLOW, small)); //7
		vcells.add(new VoronoiCell(canvas, new Point(69,233), Color.BLUE, small));
		vcells.add(new VoronoiCell(canvas, new Point(90,340), Color.GREEN, small));
		vcells.add(new VoronoiCell(canvas, new Point(200,300), Color.RED, large)); //10
		vcells.add(new VoronoiCell(canvas, new Point(269,199), Color.BLUE, small));
		vcells.add(new VoronoiCell(canvas, new Point(310,147), Color.RED, large)); //12
		vcells.add(new VoronoiCell(canvas, new Point(223,46), Color.GREEN, small));
		vcells.add(new VoronoiCell(canvas, new Point(289,29), Color.BLUE, medium));
		vcells.add(new VoronoiCell(canvas, new Point(200,420), Color.YELLOW, small)); //15
		vcells.add(new VoronoiCell(canvas, new Point(297,359), Color.BLUE, small));
		vcells.add(new VoronoiCell(canvas, new Point(276,477), Color.GREEN, small));
		vcells.add(new VoronoiCell(canvas, new Point(331,288), Color.GREEN, small));
		vcells.add(new VoronoiCell(canvas, new Point(383,296), Color.BLUE, small));
		vcells.add(new VoronoiCell(canvas, new Point(386,390), Color.RED, large));
		vcells.add(new VoronoiCell(canvas, new Point(338,77), Color.GREEN, medium));///21
		vcells.add(new VoronoiCell(canvas, new Point(398,56), Color.BLUE, 40));
		vcells.add(new VoronoiCell(canvas, new Point(460,164), Color.YELLOW, small)); //23
		vcells.add(new VoronoiCell(canvas, new Point(433,295), Color.GREEN, small));
		vcells.add(new VoronoiCell(canvas, new Point(479,347), Color.BLUE, small));
		vcells.add(new VoronoiCell(canvas, new Point(524,414), Color.YELLOW, small)); //25
		vcells.add(new VoronoiCell(canvas, new Point(596,448), Color.GREEN, small));
		vcells.add(new VoronoiCell(canvas, new Point(598,330), Color.RED, medium));
		vcells.add(new VoronoiCell(canvas, new Point(509,262), Color.BLUE, small));//29
		vcells.add(new VoronoiCell(canvas, new Point(568,217), Color.GREEN, medium));
		vcells.add(new VoronoiCell(canvas, new Point(569,155), Color.BLUE, medium)); //31
		vcells.add(new VoronoiCell(canvas, new Point(475,47), Color.GREEN, small));
		vcells.add(new VoronoiCell(canvas, new Point(554,50), Color.BLUE, small));
		vcells.add(new VoronoiCell(canvas, new Point(642,108), Color.GREEN, small)); //34
		vcells.add(new VoronoiCell(canvas, new Point(626,258), Color.YELLOW, small)); //35
		vcells.add(new VoronoiCell(canvas, new Point(695,393), Color.GREEN, small));
		vcells.add(new VoronoiCell(canvas, new Point(681,480), Color.YELLOW, small)); //37
		vcells.add(new VoronoiCell(canvas, new Point(702,310), Color.BLUE, small));
		vcells.add(new VoronoiCell(canvas, new Point(711,193), Color.RED, large));
		vcells.add(new VoronoiCell(canvas, new Point(728,76), Color.BLUE, small));
		vcells.add(new VoronoiCell(canvas, new Point(667,37), Color.YELLOW, small)); //41
		vcells.add(new VoronoiCell(canvas, new Point(781,299), Color.YELLOW, small)); //42
		
		//blockers
		VoronoiCell firstBlocker = new VoronoiCell(canvas, new Point(173,28), Color.BLACK, small);
		vcells.add(firstBlocker);
		vcells.add(new VoronoiCell(canvas, new Point(26,293), Color.BLACK, small));
		vcells.add(new VoronoiCell(canvas, new Point(553,107), Color.BLACK, small));
		vcells.add(new VoronoiCell(canvas, new Point(263,406), Color.BLACK, small));
		vcells.add(new VoronoiCell(canvas, new Point(130,400), Color.BLACK, small));
		vcells.add(new VoronoiCell(canvas, new Point(283,271), Color.BLACK, small));
		vcells.add(new VoronoiCell(canvas, new Point(554,380), Color.BLACK, small));
		vcells.add(new VoronoiCell(canvas, new Point(479,299), Color.BLACK, small));
		vcells.add(new VoronoiCell(canvas, new Point(646,424), Color.BLACK, small));
		vcells.add(new VoronoiCell(canvas, new Point(104,278), Color.BLACK, small));
		vcells.add(new VoronoiCell(canvas, new Point(575,261), Color.BLACK, small));
		
		//also blocker-ek
		vcells.add(new VoronoiCell(canvas, new Point(40,540), Color.BLACK, small));
		vcells.add(new VoronoiCell(canvas, new Point(40,680), Color.BLACK, medium));
		vcells.add(new VoronoiCell(canvas, new Point(200,540), Color.BLACK, small));
		vcells.add(new VoronoiCell(canvas, new Point(200,680), Color.BLACK, medium));
		vcells.add(new VoronoiCell(canvas, new Point(400,540), Color.BLACK, small));
		vcells.add(new VoronoiCell(canvas, new Point(400,680), Color.BLACK, medium));
		vcells.add(new VoronoiCell(canvas, new Point(600,540), Color.BLACK, small));
		vcells.add(new VoronoiCell(canvas, new Point(600,680), Color.BLACK, medium));
		vcells.add(new VoronoiCell(canvas, new Point(760,540), Color.BLACK, small));
		vcells.add(new VoronoiCell(canvas, new Point(760,680), Color.BLACK, medium));
		
		
		//végigmegy a "blocker" cellákon és inaktívvá teszi őket.
		for (int i = vcells.indexOf(firstBlocker); i < vcells.size(); i++) {
			vcells.get(i).disable();
		}
		
		
		return vcells;
	}
	
	/**
	 * Beállítja egy Voronoi cellának a vele szomszédos cellákat
	 * Itt a kölcsönös szomszédosság garantált, tehát a szomszédosségi kapcsolatok
	 * nem egyeznek meg a játék modelljében lévő mezők szomszédossági kapcsolataival.
	 */
	private void setNeighbours() {
		for (VoronoiCell c : cells) {
			HashSet<Point> pixels = c.getPixels();
			for (Point p : pixels) {
				
				//szomszedok a 4 iranyban
				Point right = new Point(p.x + 1, p.y);
				Point left = new Point(p.x - 1, p.y);
				Point up = new Point(p.x, p.y + 1);
				Point down = new Point(p.x, p.y - 1);
				
				//Ha legalább az egyik iránmyban nem a saját pixelünk van
				if (!pixels.contains(right) || !pixels.contains(left) || !pixels.contains(up) || !pixels.contains(down)) {
					for (VoronoiCell potentialNeighbour : cells) {
						//saját magunkat nem addoljuk be szomszédnak
						//És inaktív cellát sem addolunk be szomszédnak
						if (!c.equals(potentialNeighbour) && !potentialNeighbour.isDisabled()) {
							if(potentialNeighbour.getPixels().contains(right) || potentialNeighbour.getPixels().contains(left)
									|| potentialNeighbour.getPixels().contains(up) || potentialNeighbour.getPixels().contains(down)) {
								c.addNeighbour(potentialNeighbour);
								System.out.println("added neighbour");	
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Timer hívja időközönként (az animációhoz késleltetés).
	 * Növeli a cellák méretét.
	 * Ha befejezi a pályagenerálást megállítja a timert.
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		distance++;
		if(expandCells()) {
			setNeighbours();
			System.out.println("neighbours added");
			timer.stop();
			System.out.println("Timer stopped");
			
		}
		System.out.println(distance);
		repaint();
	}

	boolean neighboursShown = false;
	@Override
	public void mouseClicked(MouseEvent arg0) {
		
		//ha már highlighted és kattintást észlelünk, akkor visszarajzoljuk az eredeti színre
		for (VoronoiCell c : cells) {
			if (neighboursShown) {
				c.redraw();
				repaint();
				System.out.println("cell redrawn");
			}
		}
		neighboursShown = false;
		
		
		for (VoronoiCell c : cells) {
			//megkeressük a kattintott cella szomszédait és highlightoljuk oket
			if(c.getPixels().contains(arg0.getPoint())) {
				for(VoronoiCell neighbour : c.getNeighbours()) {
					neighbour.redraw(Color.GREEN.darker());
					neighboursShown = true;
					repaint();
					System.out.println("cell drawn in green");
				}
			}
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		for (VoronoiCell c : cells) {
				c.redraw();
				repaint();
		}
		
		for (VoronoiCell c : cells) {
			if (c.getPixels().contains(arg0.getPoint())) {
				c.highlight();
				repaint();
				break;
			}
		}
		
	}	
}